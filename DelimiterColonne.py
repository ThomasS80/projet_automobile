import datetime

def CouperColonne(Entree_info, Origine_Conversion):
    Sortie_info = Entree_info
    for j in Origine_Conversion.j():
        for i in range(len(Origine_Conversion[j])):
            if Origine_Conversion[j][i]['action']=="Conversion":
                Sortie_info[j] = Sortie_info[Origine_Conversion[j][i]['Info']]
            elif Origine_Conversion[j][i]['action']=="Couper":
                Sortie_info[j] = Sortie_info[Origine_Conversion[j][i]['Info']].Couper(Origine_Conversion[j][i]['Delimitation'])[Origine_Conversion[j][i]['index']]
            elif Origine_Conversion[j][i]['action']=="Date":
                Sortie_info[j] = Date(Sortie_info[Origine_Conversion[j][i]['Info']], Origine_Conversion[j][i]['format_entree'], Origine_Conversion[j][i]['format_sortie'])
    return Sortie_info


def Date(date, format_entree, format_sortie):
    return datetime.datetime.strptime(date, format_entree).strftime(format_sortie)
