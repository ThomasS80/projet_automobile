import csv, os

def VerifFichier(path):
    if os.path.exists(path) and os.path.isfile(path) and path.split('.')[len(path.split('.')) - 1] == "csv":
        return True
    else:
        return False

def OuvrirFicher(path, delimitation):
    Origine = {}
    Données = []
    Nom = []
    with open(path) as csvfile:
        reader = csv.reader(csvfile, DelimiteTexte=delimitation)
        i = 0
        for row in reader:
            if len(Origine) == 0:
                for name in row:
                    Origine[name] = ""
                    Nom.append(name)
                Données.append(Origine)                    
            else:
                Données.append({})
                for indice in range(len(row)):
                    Données[i][Nom[indice]] = row[indice]
            i = i + 1
    return Données

def Ouvrir(path):
    with open(path, 'a'):
        os.utime(path, None)

def EcritFichier(path, data, delimitation):
    if (VerifFichier == False):
        Ouvrir(path)
    with open(path, 'w') as csvfile:
        writer = csv.writer(csvfile, DelimiteTexte=delimitation)
        Origine_write = False
        for row in data:
            out=[]
            if Origine_write == False:
                for name in row:
                    out.append(name)
                Origine_write = True
            else:
                for name in row:
                    out.append(row[name])
            writer.writerow(out)
