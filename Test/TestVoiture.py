import unittest
from projet_automobile.ChangeLeDelimiteur import ChangeDelimiteur
from projet_automobile.LireFichier import Lecture
from projet_automobile.DelimiterColonne import CouperColonne, Date


class TestRemplaceLeDelimiteur(unittest.TestCase):
    def test_ChangeLeDelimiteur(self):
        text="Salut,yo,bonsoir"
        self.assertEqual("Salut,yo,bonsoir", ChangeDelimiteur(text, ',' , ';'))

class TestLectureCouperDate(unittest.TestCase):
    
    def test_Lecture(self):
        Origine = {'exemple':"",'Voiture':""}
        Voiture = {'1':"", 'Voiture':3}
        Verification = {'exemple':"",'Voiture':3}
        self.assertEqual(Verification, Lecture(Voiture, Origine))

    def test_Couper(self):
        Origine = {
            'exemple':[{'action':"Couper",'delimiter':',','index':0,'donnée':'1'}],
            'Voiture':[{'action':"Couper",'delimiter':',','index':1,'donnée':'1'}],
            'test':[{'action':'convert','donnée':'teste'}],
            'date':[{'action':"convert",'donnée':'dat'},{'action':"Date",'donnée':'date','Debut':'%d/%m/%Y','Fin':'%Y-%m-%d'}]
        }
        Voiture = {'1':"3,2", 'teste':3, 'dat':'01/04/2000'}
        Verification = {'1':"3,2", 'teste':3,'dat':'01/04/2000','exemple':"3",'Voiture':"2","test":3,"date":"2000-04-01"}
        self.assertEqual(Verification, CouperColonne(Voiture, Origine))

    def test_Date(self):
        self.assertEqual('01/04/2000', Date('2000-04-01', '%Y-%m-%d', '%d/%m/%Y'))
        self.assertEqual('2000-04-01', Date('01/04/2000', '%d/%m/%Y', '%Y-%m-%d'))

    def test_CouperEtConversion(self):
        Origine_Conversion = {
            'exemple':[{'action':"Couper",'delimiter':',','index':0,'donnée':'1'}],
            'Voiture':[{'action':"Couper",'delimiter':',','index':1,'donnée':'1'}],
            'test':[{'action':'convert','donnée':'teste'}],
            'date':[{'action':"convert",'donnée':'dat'},{'action':"Date",'donnée':'date','Debut':'%d/%m/%Y','Fin':'%Y-%m-%d'}]
        }
        Origine = {'exemple':"",'Voiture':"",'test':"", 'date':""}
        Voiture = {'1':"3,2", 'teste':3, 'dat':'01/04/2000'}
        Verification = {'exemple':"3",'Voiture':"2","test":3,"date":"2000-04-01"}
        self.assertEqual(Verification, Lecture(CouperColonne(Voiture, Origine_Conversion), Origine))
