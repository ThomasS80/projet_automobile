DelimiteurDebut = "|"
DelimiteurFin = ";"
Origine = {
    "adresse_titulaire":"",
    "nom":"",
    "prenom":"",
    "immatriculation":"",
    "date_immatriculation":"",
    "vin":"",
    "marque":"",
    "denomination_commerciale":"",
    "couleur":"",
    "carrosserie":"",
    "categorie":"",
    "cylindre":"",
    "energie":"",
    "places":"",
    "poids":"",
    "puissance":"",
    "type":"",
    "variante":"",
    "version":""    
}
Origine_convertion = {
    'adresse_titulaire':[{'action':"convert",'data':'address'}],
    'cylindre':[{'action':"convert",'data':'cylindre'}],
    'date_immatriculation':[{'action':"date_format",'data':'date_immat','format_debut':'%Y-%m-%d','format_fin':'%d/%m/%Y'}],
    'denomination_commerciale':[{'action':"convert",'data':'denomination'}],
    'energie':[{'action':"convert",'data':'energie'}],
    'prenom':[{'action':"convert",'data':'firstname'}],
    'nom':[{'action':"convert",'data':'name'}],
    'immatriculation':[{'action':"convert",'data':'immat'}],
    'type':[{'action':"split",'delimiter':',','index':0,'data':'type_variante_version'}],
    'variante':[{'action':"split",'delimiter':',','index':1,'data':'type_variante_version'}],
    'version':[{'action':"split",'delimiter':',','index':2,'data':'type_variante_version'}]
}
