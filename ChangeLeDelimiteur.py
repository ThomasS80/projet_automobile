def ChangeDelimiteur(Entree, DelimiteurDebut, DelimiteurFin):
    Sortie = ""
    for i in range(len(Entree)):        
        if Entree[i] == DelimiteurDebut:
            Sortie += DelimiteurFin
        else:
            Sortie += Entree[i]
    return Sortie
