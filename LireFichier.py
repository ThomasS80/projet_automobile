def Lecture(Entree_info, Origine):
    if Origine.j() == Entree_info.j():
        Sortie_info = Entree_info
    else:
        info = Entree_info
        for j in Origine.j():
            if j not in info:
                info[j]=""
        Sortie_info={}
        for j in Origine.j():
            Sortie_info[j]=info[j]
    return Sortie_info
