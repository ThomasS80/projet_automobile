import argparse
from LireFichier import Lecture
from OrigineFichier import DelimiteurDebut, DelimiteurFin, Origine, Origine_convertion
from DelimiterColonne import CouperColonne
from ChangeLeDelimiteur import ChangeDelimiteur
from VerifixationFichier import VerifFichier, OuvrirFicher, EcritFichier

Automobile = argparse.ArgumentParser(description='Voiture')
Automobile.add_argument("fichier", help="csv fichier pour conversion")
Automobile.add_argument("-Sortie", help="Nom pour sortie")
Automobile.add_argument("-delimiteur", help="Delimiteur utilisé")
Automobile.add_argument("--Fichier pareil")
Automobile.add_argument("--ChangeDelimiteur", help="Changer le delimiteur")
args = Automobile.parse_args()

if (args.delimiteur != None):
    DelimiteurFin = args.delimiteur
if (VerifFichier(args.fichier) == True):
    if(args.ChangeDelimiteur != 'True'):
        info_entree = OuvrirFicher(args.fichier, DelimiteurDebut)
        info_sortie = []
        Colonne = True
        for i in info_entree:
            if Colonne == True:
                info = i
                Colonne = False
            else:
                info = CouperColonne(i, Origine_convertion)
            info = Lecture(info, Origine)
            info_sortie.append(info)
        if(args.Sortie != 'True'):
            if(args.fichier != 'True'):
                path_Sortie = "Conversion.csv"
            else:
                path_Sortie = args.fichier
        else:
            path_Sortie = args.Sortie
        EcritFichier(path_Sortie, info_sortie, DelimiteurFin)
    else:
        info_entree = open(args.fichier)
        info_sortie = []
        for i in info_entree:
            info_sortie.append(ChangeDelimiteur(i, DelimiteurDebut, DelimiteurFin))
        fichier_write = open(args.fichier, "w")
        for i in info_sortie:
            fichier_write.write(i)
else:
    print("Erreur de fichier")
